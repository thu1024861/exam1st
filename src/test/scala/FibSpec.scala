/**
  * Created by mark on 13/03/2017.
  */
import Questions._
import org.scalatest.FunSuite
/** 15%
  * 請寫出費利曼數列1,1,2,3,5,8,....
  * ex:
  * fib(1)=1
  * fib(2)=1
  * fib(3)=2
  * fib(n)=fib(n)+fib(n-1)
  **/
class FibSpec extends FunSuite{
  test("fib(1)=1") {
    assert(fib(1)==1)

  }

  test("fib(3)=2") {
    assert(fib(3)==2)

  }
  test("fib(5)=5") {
    assert(fib(5)==5)

  }
  test("fib(10)=55") {
    assert(fib(5)==5)

  }
  test("fib(17)=1597") {
    assert(fib(17)==1597)

  }

}

